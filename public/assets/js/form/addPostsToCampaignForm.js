var $collectionHolder;

// setup an "add a tag" link
var $addTagButton = $('<button type="button" class="add_post_link btn btn-secondary">Add a post</button>');
var $newLinkLi = $('<li></li>').append($addTagButton);

$(document).ready(function() {
    // Get the ul that holds the collection of posts
    $collectionHolder = $('ul.posts');

    // add the "add a post" anchor and li to the posts ul
    $collectionHolder.append($newLinkLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find('li').length);

    $addTagButton.on('click', function () {
        // add a new post form (see next code block)
        var postQuantityLimit = parseInt($('span#postQuantity').text(), 10);
        var postFormsQuantity = $collectionHolder.find('li').length;

        if (postFormsQuantity <= postQuantityLimit) {
            addPostForm($collectionHolder, $newLinkLi);
        } else {
            $addTagButton.remove();
        }
    });
});

function addPostForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    var newForm = prototype;
    // You need this only if you didn't set 'label' => false in your tags field in ManageCampaignType
    // Replace '__name__label__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // newForm = newForm.replace(/__name__label__/g, index);

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a post" link li
    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);
}
