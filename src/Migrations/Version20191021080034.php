<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191021080034 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE post (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, campaign_id INTEGER DEFAULT NULL, link VARCHAR(255) NOT NULL, nick VARCHAR(255) NOT NULL, enter_date DATETIME NOT NULL)');
        $this->addSql('CREATE INDEX IDX_5A8A6C8DF639F774 ON post (campaign_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__campaign AS SELECT id, name, start_date, end_date FROM campaign');
        $this->addSql('DROP TABLE campaign');
        $this->addSql('CREATE TABLE campaign (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, agent_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, posts_quantity INTEGER DEFAULT NULL, pid CLOB DEFAULT NULL, CONSTRAINT FK_1F1512DD3414710B FOREIGN KEY (agent_id) REFERENCES agent (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO campaign (id, name, start_date, end_date) SELECT id, name, start_date, end_date FROM __temp__campaign');
        $this->addSql('DROP TABLE __temp__campaign');
        $this->addSql('CREATE INDEX IDX_1F1512DD3414710B ON campaign (agent_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE post');
        $this->addSql('DROP INDEX IDX_1F1512DD3414710B');
        $this->addSql('CREATE TEMPORARY TABLE __temp__campaign AS SELECT id, name, start_date, end_date FROM campaign');
        $this->addSql('DROP TABLE campaign');
        $this->addSql('CREATE TABLE campaign (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL)');
        $this->addSql('INSERT INTO campaign (id, name, start_date, end_date) SELECT id, name, start_date, end_date FROM __temp__campaign');
        $this->addSql('DROP TABLE __temp__campaign');
    }
}
