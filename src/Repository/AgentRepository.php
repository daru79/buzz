<?php

namespace App\Repository;

use App\Entity\Agent;
use App\Entity\Campaign;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AgentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Agent::class);
    }

    public function findAssignedAgentEmailByCampaignPid (string $pid): ?Agent
    {
        return $this->createQueryBuilder('agent')
            ->select('agent')
            ->join('agent.campaigns', 'campaign')
            ->where('campaign.pid = :pid')
            ->setParameter('pid', $pid)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
