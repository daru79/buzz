<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ValidPostQuantityClassValidator extends ConstraintValidator
{
    public function validate($campaign, Constraint $constraint)
    {
        if ($campaign->getPostsQuantity() < $campaign->getPosts()->count()) {
            $this->context
                ->buildViolation($constraint->message)
                ->atPath('postsQuantity')
                ->addViolation();
        }
    }
}