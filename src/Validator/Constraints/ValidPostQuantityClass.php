<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ValidPostQuantityClass extends Constraint
{
    public $message = 'Cannot decrease number of posts';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}