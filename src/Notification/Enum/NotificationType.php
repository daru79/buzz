<?php

namespace App\Notification\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static NotificationType SUCCESS()
 * @method static NotificationType ERROR()
 * @method static NotificationType WARNING()
 * @method static NotificationType INFO()
 */
class NotificationType extends Enum
{
    private const SUCCESS = 'success';
    private const ERROR = 'error';
    private const WARNING = 'warning';
    private const INFO = 'info';
}
