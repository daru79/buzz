<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Validator\Constraints as AppAssert;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CampaignRepository")
 * @AppAssert\ValidPostQuantityClass()
 */
class Campaign
{
    /**
    * @ORM\Id()
    * @ORM\GeneratedValue()
    * @ORM\Column(type="integer")
    */
    private $id;

    /**
    * @ORM\Column(type="string", length=255)
    * @Assert\Regex(
    *     pattern="/[^\w #]+/",
    *     match=false,
    *     message="Your name cannot contain special characters"
    * )
    * @Assert\NotBlank
    */
    private $name;

    /**
    * @ORM\Column(type="datetime")
    * @Assert\NotBlank
    * @Assert\GreaterThan("-1 year")
    * @Assert\Type("\DateTime")
    */
    private $startDate;

    /**
    * @ORM\Column(type="datetime")
    * @Assert\NotBlank
    * @Assert\GreaterThan(propertyPath="startDate")
    * @Assert\Type("\DateTime")
    */
    private $endDate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank
     * @Assert\Type(type="integer")
     * @Assert\Range(min=1, minMessage="A campaign should have at least one post.")
     */
    private $postsQuantity;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $pid;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expiresAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Agent", inversedBy="campaigns")
     */
    private $agent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="campaign", cascade={"persist"})
     * @Assert\Valid()
     */
    private $posts;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
        $this->postsQuantity = 1;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function setStartDate(?\DateTimeInterface $startDate): void
    {
        $this->startDate = $startDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): void
    {
        $this->endDate = $endDate;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getStartDate(): ?\DateTime
    {
        return $this->startDate;
    }

    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    public function getPostsQuantity(): ?int
    {
        return $this->postsQuantity;
    }

    public function setPostsQuantity(?int $postsQuantity): void
    {
        $this->postsQuantity = $postsQuantity;
    }

    public function getPid(): ?string
    {
        return $this->pid;
    }

    public function setPid(?string $pid): void
    {
        $this->pid = $pid;
    }

    public function getNotice(): ?string
    {
        return $this->notice;
    }

    public function setNotice(?string $notice): self
    {
        $this->notice = $notice;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getExpiresAt(): ?\DateTimeInterface
    {
        return $this->expiresAt;
    }

    public function setExpiresAt(?\DateTimeInterface $expiresAt): self
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    public function getAgent(): ?Agent
    {
        return $this->agent;
    }

    public function setAgent(?Agent $agent): self
    {
        $this->agent = $agent;

        return $this;
    }

    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): void
    {
        $this->posts->add($post);
        $post->setCampaign($this);
    }

    public function removePost(Post $post): void
    {
        $this->posts->removeElement($post);
    }

    public function isExpired(): bool
    {
        if (new \DateTime('now') >= $this->getExpiresAt()) {
            return true;
        }

        return false;
    }
}