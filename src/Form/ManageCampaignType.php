<?php

namespace App\Form;

use App\Entity\Agent;
use App\Entity\Campaign;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ManageCampaignType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class)
            ->add(
                'startDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'placeholder' => 'yyyy-MM-dd',
                    'attr' => ['class' => 'js-datepicker'],
                    'html5' => false
                ]
            )
            ->add(
                'endDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'placeholder' => 'yyyy-MM-dd',
                    'attr' => ['class' => 'js-datepicker'],
                    'html5' => false
                ]
            )
            ->add('postsQuantity', IntegerType::class)
            ->add('agent', EntityType::class, [
                'class' => Agent::class,
                'choice_label' => 'fullName',
                'placeholder' => 'Choose an Agent',
            ])
            ->add('notice', TextareaType::class)
            ->add('save', SubmitType::class, [
                'label' => 'Save Campaign',
                'attr' => ['class' => 'submit', 'disabled' => '']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Campaign::class
        ]);
    }
}