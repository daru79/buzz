<?php

namespace App\Form;

use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('link', TextType::class)
            ->add('nick', TextType::class)
            ->add(
                'enterDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'placeholder' => 'yyyy-MM-dd',
                    'attr' => ['class' => 'js-datepicker'],
                    'html5' => false
                ]
            )
            ->add('category', ChoiceType::class, [
                'choices' => [
                    'Post' => Post::CAT_POST,
                    'Catalog' => Post::CAT_CATALOG
                ]
            ])
            ->add('submit', SubmitType::class, ['label' => 'Save']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class
        ]);
    }
}