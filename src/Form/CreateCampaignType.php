<?php

namespace App\Form;

use App\Entity\Campaign;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateCampaignType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class)
            ->add(
                'startDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'placeholder' => 'yyyy-MM-dd',
                    'attr' => ['class' => 'js-datepicker'],
                    'html5' => false
                ],
            )
            ->add(
                'endDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'placeholder' => 'yyyy-MM-dd',
                    'attr' => ['class' => 'js-datepicker'],
                    'html5' => false
                ]
            )
            ->add('notice', TextareaType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Campaign']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Campaign::class
        ]);
    }
}