<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFactory
{
    private const DEFAULT_NAME = 'Unknown';

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var UserRepository */
    private $userRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        UserRepository $userRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    public function createAdmin(
        string $email,
        string $password,
        string $firstName
    ): User
    {
        $user = new User($email);

        $user->setPlainPassword($password);
        $user->setFirstName($firstName ?? self::DEFAULT_NAME);
        $user->setRoles(['ROLE_ADMIN']);

        return $user;
    }
}