<?php

namespace App\Service;

use Ramsey\Uuid\Uuid;

final class UuidGenerator implements IdGeneratorInterface
{
    public function getId(): string
    {
        return Uuid::uuid4();
    }
}