<?php

namespace App\Service;

interface IdGeneratorInterface
{
    public function getId(): string;
}