<?php

namespace App\Service;

class TokenGenerator implements TokenGeneratorInterface
{
    private const BYTES_LENGTH = 5;

    public function getToken(): string
    {
        return bin2hex(random_bytes(self::BYTES_LENGTH));
    }
}