<?php

namespace App\Service;

interface TokenGeneratorInterface
{
    public function getToken(): string;
}