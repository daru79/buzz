<?php

namespace App\Service;

use App\Entity\User;

interface UserStorageInteface
{
    public function save(User $user): void;
}