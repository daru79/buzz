<?php

namespace App\Command;

use App\Service\UserFactory;
use App\Service\UserStorageInteface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateAdminCommand extends Command
{
    /** @var UserPasswordEncoderInterface */
    private $passwordEncoder;

    /** @var UserFactory */
    private $userFactory;

    /** @var ValidatorInterface */
    private $validator;

    /** @var UserStorageInteface */
    private $userStorage;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        UserFactory $userFactory,
        ValidatorInterface $validator,
        UserStorageInteface $userStorage
    )
    {
        parent::__construct();
        $this->passwordEncoder = $passwordEncoder;
        $this->userFactory = $userFactory;
        $this->validator = $validator;
        $this->userStorage = $userStorage;
    }

    protected function configure()
    {
        $this
            ->setName('app:create-admin')
            ->setDescription('Create a new admin account.')
            ->setHelp('This command allows you to create a brand new admin account.')
            ->addArgument('email', InputArgument::REQUIRED, 'Provide Your e-mail')
            ->addArgument('password', InputArgument::REQUIRED, 'Provide a secret password')
            ->addArgument('firstName', InputArgument::OPTIONAL, 'Provide Your name (optional)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '===================',
            '  Account creator  ',
            '==================='
        ]);

        $user = $this->userFactory->createAdmin(
            $input->getArgument('email'),
            $input->getArgument('password'),
            $input->getArgument('firstName')
        );

        try {
            $errors = $this->validator->validate($user);

            if (count($errors) > 0) {
                $errorsString = (string) $errors;
                throw new \Exception($errorsString);
            }

            $this->userStorage->save($user);

            $output->writeln([
                'New accont created for:',
                '=======================',
                $user->getEmail(),
                '======================='
            ]);
        } catch (\Exception $exception) {
            $output->writeln([
                $exception->getMessage(),
                '===================='
            ]);
        }
    }
}