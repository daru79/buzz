<?php

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class AgentAccessDTO
{
    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     */
    public $email;
}