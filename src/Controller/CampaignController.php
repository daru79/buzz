<?php

namespace App\Controller;

use App\Entity\Agent;
use App\Form\CreateCampaignType;
use App\Entity\Campaign;
use App\Form\LoginAgentType;
use App\Form\ManageCampaignPostsType;
use App\Form\ManageCampaignType;
use App\Notification\Enum\NotificationType;
use App\Repository\AgentRepository;
use App\Repository\CampaignRepository;
use App\Service\IdGeneratorInterface;
use App\Service\TokenGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CampaignController extends AbstractController
{
    private const TOKEN_EXPIRE = '2 hours';

    /**
     * @Route("/admin/campaign/show", name="campaign_show")
     */
    public function show(CampaignRepository $campaignRepository): Response
    {
        $campaigns = $campaignRepository->findAll();

        return $this->render(
            'Campaign/showCampaign.html.twig',
            [
                'campaigns' => $campaigns
            ]
        );
    }

    /**
    * @Route("/admin/campaign/create", name="campaign_create")
    */
    public function create(
        Request $request,
        EntityManagerInterface $entityManager,
        IdGeneratorInterface $idGenerator
    ): Response {
        $form = $this->createForm(CreateCampaignType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $campaign = $form->getData();
            $campaign->setPid($idGenerator->getId());

            $entityManager->persist($campaign);
            $entityManager->flush();

            $this->addFlash(
                NotificationType::SUCCESS()->getValue(),
                sprintf('Campaign <i>%s</i> created!', $campaign->getName())
            );

            return $this->redirectToRoute('campaign_create');
        }

        return $this->render(
            'Campaign/createCampaign.html.twig',
            [
                'createCampaignForm' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/admin/campaign/manage/{id}", name="campaign_manage", requirements={"id"="\d+"})
     */
    public function manage(
        Request $request,
        EntityManagerInterface $entityManager,
        Campaign $campaign
    ): Response {
        $form = $this->createForm(ManageCampaignType::class, $campaign);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $campaign = $form->getData();

            $entityManager->flush();

            $this->addFlash(
                NotificationType::SUCCESS()->getValue(),
                sprintf('Campaign <i>%s</i> updated.', $campaign->getName())
            );

            return $this->redirectToRoute('campaign_show');
        }

        return $this->render(
            'Campaign/manageCampaign.html.twig',
            [
                'campaign' => $campaign,
                'manageCampaignForm' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/campaign/getaccess/{pid}", name="campaign_getaccess")
     */
    public function getaccess(
        Request $request,
        EntityManagerInterface $entityManager,
        AgentRepository $agentRepository,
        Campaign $campaign,
        \Swift_Mailer $mailer,
        string $pid
    ): Response {
        $accessForm = $this->createForm(LoginAgentType::class);
        $accessForm->handleRequest($request);

        if ($accessForm->isSubmitted() && $accessForm->isValid()) {
            $data = $accessForm->getData();

            $agent = $agentRepository->findAssignedAgentEmailByCampaignPid($pid);

            if ($agent instanceof Agent && $data->email === $agent->getEmail()) {
                $tokenGenerator = new TokenGenerator();

                $campaign->setToken($tokenGenerator->getToken());
                $campaign->setExpiresAt(new \DateTime(self::TOKEN_EXPIRE));

                $entityManager->persist($campaign);
                $entityManager->flush();

                $this->addFlash(
                    NotificationType::SUCCESS()->getValue(),
                    'Access token has been generated. Check Your e-mail for further instructions.'
                );

                $mailer->send($this->createEmail($agent, $campaign));

                return $this->redirectToRoute('campaign_getaccess', ['pid' => $pid]);
            }

            $this->addFlash(
                NotificationType::WARNING()->getValue(),
                'Invalid email provided'
            );

            return $this->redirectToRoute('campaign_getaccess', ['pid' => $pid]);
        }

        return $this->render(
            'Security/loginCampaign.html.twig',
            [
                'accessForm' => $accessForm->createView()
            ]
        );
    }

    /**
     * @Route("/campaign/view/{token}", name="campaign_view")
     */
    public function view(
        Request $request,
        EntityManagerInterface $entityManager,
        Campaign $campaign,
        string $token
    ): Response {
        if ($campaign->isExpired()) {
            throw $this->createNotFoundException('You have no access.');
        }

        $form = $this->createForm(ManageCampaignPostsType::class, $campaign);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $campaign = $form->getData();

            $entityManager->flush();

            $this->addFlash(
                NotificationType::SUCCESS()->getValue(),
                'Post modified.'
            );

            return $this->redirectToRoute('campaign_view', ['token' => $token]);
        }
        return $this->render(
            'Campaign/viewCampaign.html.twig',
            [
                'campaign' => $campaign,
                'manageCampaignPostsForm' => $form->createView()
            ]
        );
    }

    private function createEmail(Agent $agent, Campaign $campaign): \Swift_Message
    {
         return (new \Swift_Message('BuzzApp Campaign Access'))
            ->setFrom('send@example.com')
            ->setTo($agent->getEmail())
            ->setBody(
                $this->renderView(
                    'Email/sendToken.html.twig',
                    [
                        'token' => $campaign->getToken(),
                        'firstName' => $agent->getFirstName(),
                        'campaignName' => $campaign->getName()
                    ]
                ),
                'text/html'
            );
    }
}