<?php

namespace App\Controller;

use App\Form\CreateAgentType;
use App\Form\EditAgentType;
use App\Notification\Enum\NotificationType;
use App\Repository\AgentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class AgentController extends AbstractController
{
    /**
     * @Route("/admin/agent/show", name="agent_show")
     */
    public function show(AgentRepository $agentRepository)
    {
        $agents = $agentRepository->findAll();

        return $this->render(
            'Agent/showAgent.html.twig',
            [
                'agents' => $agents
            ]
        );
    }

    /**
     * @Route("/admin/agent/create", name="agent_create")
     */
    public function create(Request $request, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CreateAgentType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $agent = $form->getData();

            $entityManager->persist($agent);
            $entityManager->flush();

            $this->addFlash(
                NotificationType::SUCCESS()->getValue(),
                sprintf('Agent <i>%s</i> created!', $agent->getFullName())
            );

            return $this->redirectToRoute('agent_create');
        }

        return $this->render(
            'Agent/createAgent.html.twig',
            [
                'createAgentForm' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/admin/agent/edit/{id}", name="agent_edit", requirements={"id"="\d+"})
     */
    public function edit(
        Request $request,
        EntityManagerInterface $entityManager,
        AgentRepository $agentRepository,
        int $id
    ): Response {
        $agent = $agentRepository->find($id);

        if (!$agent) {
            throw new NotFoundHttpException('Invalid ID. No Agent found.');
        }

        $form = $this->createForm(editAgentType::class, $agent);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $agent = $form->getData();

            $entityManager->flush();

            $this->addFlash(
                NotificationType::SUCCESS()->getValue(),
                sprintf('Agent <i>%s</i> modified!', $agent->getFullName())
            );

            return $this->redirectToRoute('agent_show');
        }

        return $this->render(
            'Agent/editAgent.html.twig',
            [
                'editAgentForm' => $form->createView()
            ]
        );
    }
}
